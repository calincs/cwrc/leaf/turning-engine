#!/usr/bin/python3

"""Base of our XSLT API Processor"""
import asyncio
import io
import os

from typing import Optional
from fastapi import FastAPI, APIRouter, File, UploadFile, Form, HTTPException
from fastapi.responses import StreamingResponse, RedirectResponse
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from src.xslts import libxslt

app = FastAPI(
    title="Leaf Turning Engine",
    version="1.0.0",
    description="The Leaf Turning Engine API!",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Define routers for each API version
api_router_v1 = APIRouter(prefix="/v1")


@app.get("/")
async def redirect_root_to_docs():
    """Redirect a user calling the API root '/' to the API
    documentation.
    """
    return RedirectResponse(url="/docs")


@api_router_v1.post("/transform-file")
async def transform_file_v1(
    file: UploadFile = File(...),
    from_type: Optional[str] = Form(None),
    to_type: Optional[str] = Form(None),
    xslt: Optional[UploadFile] = None,
):
    """
    API endpoint for transforming files. Allowed transformations include:\n
    `TEI → HTML`\n
    `TEI → Markdown`\n
    `TRANSKRIBUS → TEI`\n
    `WORD → TEI`\n\n

    from_type, to_type, and xslt are all optional parameters, but it is\n
    recommended that users either:\n
    #1. Use `from_type` and `to_type` \n
    #2. Use a custom xslt
    """
    try:
        # Checking parameters and executing transformations
        if (
            xslt
            and (from_type is None or from_type == "")
            and (to_type is None or to_type == "")
        ):
            # code to transform the uploaded file with the given xslt
            file_content, file_extension = await asyncio.wait_for(
                libxslt.apply_xslt_from_string(
                    file.file.read().decode("utf-8"), xslt.file.read().decode("utf-8")
                ),
                timeout=45,  # set timeout to 45 seconds
            )

            # New file name
            og_filename, _ = os.path.splitext(file.filename)
            new_filename = og_filename + "_transformed." + file_extension

            # Setting up request
            headers = {"Content-Disposition": f"attachment; filename={new_filename}"}
            return StreamingResponse(
                io.BytesIO(file_content.encode("utf-8")), headers=headers
            )

        if (
            (from_type and from_type != "")
            and (to_type and to_type != "")
            and xslt is None
        ):
            # code to transform the uploaded file with the predetermined xslt
            if not libxslt.transformation_exists(from_type, to_type):
                raise HTTPException(
                    status_code=400,
                    detail=f"Invalid Request: Transformations from {from_type.upper()} "
                    f"to {to_type.upper()} is not currently supported.",
                )
            file_content, file_extension = await asyncio.wait_for(
                libxslt.transform_file(
                    file.file.read().decode("utf-8"), from_type, to_type
                ),
                timeout=45,  # set timeout to 45 seconds
            )

            # New file name
            og_filename, _ = os.path.splitext(file.filename)
            new_filename = og_filename + "_transformed." + file_extension

            # Setting up request
            headers = {"Content-Disposition": f"attachment; filename={new_filename}"}
            return StreamingResponse(
                io.BytesIO(file_content.encode("utf-8")), headers=headers
            )
        raise HTTPException(
            status_code=400,
            detail="Invalid Parameters: Required parameters are either "
            + "[file:file, from_type:str, to_type:str] or [file:file, xslt:file].",
        )
    except asyncio.TimeoutError as exception:
        raise HTTPException(
            status_code=408,
            detail="Request Timeout: The request took longer than 45 seconds to complete.",
        ) from exception
    except HTTPException:
        raise  # re-raise HTTPExceptions as is
    except Exception as exception:
        raise HTTPException(
            status_code=400, detail=f"Server error: {exception}."
        ) from exception


class Transformation(BaseModel):
    """
    Transformation model for the string transformation endpoint
    """

    input_string: str
    from_type: Optional[str] = None
    to_type: Optional[str] = None
    xslt: Optional[str] = None


# endpoint for input strings and output strings for transformations
@api_router_v1.post("/transform-string")
async def transform_string_v1(transformation: Transformation):
    """
    API endpoint for transforming strings. Allowed transformations include:\n
     `TEI → HTML`\n
    `TEI → Markdown`\n
    `TRANSKRIBUS → TEI`\n
    `WORD → TEI`\n\n
    from_type, to_type, and xslt are all optional parameters, \n
    but it is recommended that users either:\n
    #1. Use `from_type` and `to_type` \n
    #2. Use a custom xslt
    """
    try:
        # Checking parameters and executing transformations
        xslt_bool = (
            transformation.xslt and transformation.xslt != ""
        )  # check if xslt is provided
        from_bool = (
            transformation.from_type and transformation.from_type != ""
        )  # check if from_type is provided
        to_bool = (
            transformation.to_type and transformation.to_type != ""
        )  # check if to_type is provided
        if xslt_bool and not from_bool and not to_bool:
            # code to transform the uploaded file with the given xslt
            file_content, _ = await asyncio.wait_for(
                libxslt.apply_xslt_from_string(
                    transformation.input_string,
                    transformation.xslt,
                ),
                timeout=45,  # set timeout to 45 seconds
            )

            # Send the response back of just the string
            return {"transformed_string": file_content}

        if not xslt_bool and from_bool and to_bool:
            # code to transform the uploaded file with the predetermined xslt
            if not libxslt.transformation_exists(
                transformation.from_type, transformation.to_type
            ):
                raise HTTPException(
                    status_code=400,
                    detail="Invalid Request: Transformations"
                    + f" from {transformation.from_type.upper()} "
                    f"to {transformation.to_type.upper()} is not currently supported.",
                )
            file_content, _ = await asyncio.wait_for(
                libxslt.transform_file(
                    transformation.input_string,
                    transformation.from_type,
                    transformation.to_type,
                ),
                timeout=45,  # set timeout to 45 seconds
            )

            # Send the response back of just the string
            return {"transformed_string": file_content}

        raise HTTPException(
            status_code=400,
            detail="Invalid Parameters: Required parameters are either "
            + "[input_string:str, from_type:str, to_type:str] or [input_string:str, xslt:file].",
        )
    except asyncio.TimeoutError as exception:
        raise HTTPException(
            status_code=408,
            detail="Request Timeout: The request took longer than 45 seconds to complete.",
        ) from exception
    except HTTPException:
        raise
    except Exception as exception:
        raise HTTPException(
            status_code=400, detail=f"Server error: {exception}."
        ) from exception


# Endpoint for listing available transformations
@api_router_v1.get("/list-transformations")
async def list_transformations_v1():
    """
    API endpoint for listing available transformations. Allowed transformations include:\n
     `TEI → HTML`\n
    `TEI → Markdown`\n
    `TRANSKRIBUS → TEI`\n
    `WORD → TEI`\n\n
    """
    try:
        return libxslt.get_transformations()
    except Exception as exception:
        raise HTTPException(
            status_code=400, detail=f"Server error: {exception}."
        ) from exception


# Mount routers to the main app
app.include_router(api_router_v1)
