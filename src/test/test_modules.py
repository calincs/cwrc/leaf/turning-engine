"""Testing of the libxslt.py functions."""

import asyncio
import pytest

from saxonche import PySaxonApiError

from src.xslts import libxslt


def test_transformation_exists():
    """Testing incorrect parameters for transformation_exsists"""
    assert libxslt.transformation_exists("TEI", "HTML")
    assert libxslt.transformation_exists("Transkribus", "TEI")
    assert not libxslt.transformation_exists("TEI", "FakeConversion")
    assert not libxslt.transformation_exists("", "")


@pytest.mark.asyncio
async def test_transform_file():
    """Testing incorrect parameters for transformation_file"""
    # Fake file string, but real parameters
    try:
        hold = await asyncio.wait_for(
            libxslt.transform_file("test", "TEI", "HTML"), timeout=45
        )
        # It won't reach here
        assert hold is not None
    except PySaxonApiError as xmlerror:
        assert (
            xmlerror is not None
        ), f"Exception {type(xmlerror).__name__} was raised with message: {str(xmlerror)}"
