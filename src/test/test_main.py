#!/usr/bin/python3
"""Testing endpoints for the API"""
import os
import base64
import json

from fastapi.testclient import TestClient
from src.main import app

client = TestClient(app)


def test_transkribus_up1b_success_fileendpoint():
    """User Profile 1b - experience (Transkribus to LWC) - V1"""
    file_path = os.path.join(os.path.dirname(__file__), "ExampleTranskribus.xml")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-file",
            files={
                "file": ("ExampleTranskribus.xml", file, "multipart/form-data"),
                "from_type": (None, "TRANSKRIBUS"),
                "to_type": (None, "TEI"),
            },
        )
    assert response.status_code == 200


def test_invalid_transformation_with_wrong_params_fileendpoint():
    """Incorrect parameters test (wrong conversion types)."""
    file_path = os.path.join(os.path.dirname(__file__), "ExampleTranskribus.xml")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-file",
            files={
                "file": ("ExampleTranskribus.xml", file, "multipart/form-data"),
                "from_type": (None, "fake"),
                "to_type": (None, "params"),
            },
        )
    assert response.status_code == 400
    assert response.content.decode() == (
        '{"detail":"Invalid Request: Transformations from '
        'FAKE to PARAMS is not currently supported."}'
    )


def test_transform_v1_with_invalid_params_fileendpoint():
    """Testing invalid conversion type parameters (only from_type but no to_type)."""
    file_path = os.path.join(os.path.dirname(__file__), "ExampleTranskribus.xml")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-file",
            files={
                "file": ("ExampleTranskribus.xml", file, "multipart/form-data"),
                "from_type": (None, "transkribus"),
            },
        )
    assert response.status_code == 400
    assert response.content.decode() == (
        '{"detail":"Invalid Parameters: Required parameters are either '
        '[file:file, from_type:str, to_type:str] or [file:file, xslt:file]."}'
    )


def test_invalid_transformation_send_no_file_fileendpoint():
    """Sending no file and just from and to parameters."""
    response = client.post(
        "/v1/transform-file",
        data={
            "from_type": "transkribus",
            "to_type": "tei",
        },
    )
    print(response)
    # Unprocessable Content
    assert response.status_code == 422
    content = json.loads(response.content.decode())
    assert content["detail"][0]["loc"] == ["body", "file"]
    assert content["detail"][0]["msg"] == "Field required"
    assert content["detail"][0]["type"] == "missing"


def test_file_random_file_fileendpoint():
    """Testing with a .png file to see if the code throws an error."""
    file_path = os.path.join(os.path.dirname(__file__), "FlexLogoBlack.png")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-file",
            files={
                "file": ("FlexLogoBlack.png", file, "multipart/form-data"),
                "from_type": (None, "TEI"),
                "to_type": (None, "HTML"),
            },
        )
    # Server error. Caught by the try and except wrapped around the endpoint.
    assert response.status_code == 400


def test_transform_lwc_up2a_fileendpoint():
    """User Profile 2a - experience (LWC to HTML) - V1"""
    file_path = os.path.join(os.path.dirname(__file__), "ExampleTEI.xml")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-file",
            files={
                "file": ("ExampleTEI.xml", file, "multipart/form-data"),
                "from_type": (None, "TEI"),
                "to_type": (None, "HTML"),
            },
        )
    # Should return success
    assert response.status_code == 200


def test_custom_xslt_success_fileendpoint():
    """Testing custom xslt transformation"""
    to_convert_path = os.path.join(os.path.dirname(__file__), "ExampleTEI.xml")
    xslt_path = os.path.join(os.path.dirname(__file__), "TEI2TXT.xsl")
    with open(to_convert_path, "rb") as to_convert_file:
        with open(xslt_path, "rb") as xslt_file:
            response = client.post(
                "/v1/transform-file",
                files={
                    "file": (
                        "ExampleTEI.xml",
                        to_convert_file.read(),
                        "multipart/form-data",
                    ),
                    "from_type": (None, ""),
                    "to_type": (None, ""),
                    "xslt": (
                        "TEI2TXT.xsl",
                        xslt_file.read().decode("utf-8"),
                        "multipart/form-data",
                    ),
                },
            )
    assert response.status_code == 200


#
# V1 String Endpoint Tests
#


def test_transkribus_up1b_success_stringendpoint():
    """User Profile 1b - experience (Transkribus to LWC) - V1"""
    file_path = os.path.join(os.path.dirname(__file__), "ExampleTranskribus.xml")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-string",
            json={
                "input_string": file.read().decode("utf-8"),
                "from_type": "TRANSKRIBUS",
                "to_type": "TEI",
            },
        )
    assert response.status_code == 200


def test_invalid_transformation_with_wrong_params_stringendpoint():
    """Incorrect parameters test (wrong conversion types)."""
    file_path = os.path.join(os.path.dirname(__file__), "ExampleTranskribus.xml")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-string",
            json={
                "input_string": file.read().decode("utf-8"),
                "from_type": "fake",
                "to_type": "params",
            },
        )
    assert response.status_code == 400
    assert response.content.decode() == (
        '{"detail":"Invalid Request: Transformations from '
        'FAKE to PARAMS is not currently supported."}'
    )


def test_transform_v1_with_invalid_params_stringendpoint():
    """Testing invalid conversion type parameters (only from_type but no to_type)."""
    file_path = os.path.join(os.path.dirname(__file__), "ExampleTranskribus.xml")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-string",
            json={
                "input_string": file.read().decode("utf-8"),
                "from_type": "transkribus",
            },
        )
    assert response.status_code == 400
    assert response.content.decode() == (
        '{"detail":"Invalid Parameters: Required parameters are either '
        '[input_string:str, from_type:str, to_type:str] or [input_string:str, xslt:file]."}'
    )


def test_invalid_transformation_send_no_file_stringendpoint():
    """Sending no file and just from and to parameters."""
    response = client.post(
        "/v1/transform-string",
        json={
            "from_type": "transkribus",
            "to_type": "tei",
        },
    )
    # Unprocessable Content
    assert response.status_code == 422
    content = json.loads(response.content.decode())
    assert content["detail"][0]["loc"] == ["body", "input_string"]
    assert content["detail"][0]["msg"] == "Field required"
    assert content["detail"][0]["type"] == "missing"
    assert content["detail"][0]["input"]["from_type"] == "transkribus"
    assert content["detail"][0]["input"]["to_type"] == "tei"


def test_file_random_file_stringendpoint():
    """Testing with a .png file to see if the code throws an error."""
    file_path = os.path.join(os.path.dirname(__file__), "FlexLogoBlack.png")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-string",
            json={
                "input_string": str(base64.b64encode(file.read())),
                "from_type": "TEI",
                "to_type": "HTML",
            },
        )
    # Server error. Caught by the try and except wrapped around the endpoint.
    assert response.status_code == 400


def test_transform_lwc_up2a_stringendpoint():
    """User Profile 2a - experience (LWC to HTML) - V1"""
    file_path = os.path.join(os.path.dirname(__file__), "ExampleTEI.xml")
    with open(file_path, "rb") as file:
        response = client.post(
            "/v1/transform-string",
            json={
                "input_string": file.read().decode("utf-8"),
                "from_type": "TEI",
                "to_type": "HTML",
            },
        )
    # Should return success
    assert response.status_code == 200
    assert json.loads(response.content)["transformed_string"].startswith(
        "<!DOCTYPE html"
    )


def test_custom_xslt_success_stringendpoint():
    """Testing custom xslt transformation"""
    to_convert_path = os.path.join(os.path.dirname(__file__), "ExampleTEI.xml")
    xslt_path = os.path.join(os.path.dirname(__file__), "TEI2TXT.xsl")
    with open(to_convert_path, "rb") as to_convert_file:
        with open(xslt_path, "rb") as xslt_file:
            response = client.post(
                "/v1/transform-string",
                json={
                    "input_string": to_convert_file.read().decode("utf-8"),
                    "from_type": "",
                    "to_type": "",
                    "xslt": xslt_file.read().decode("utf-8"),
                },
            )
    assert response.status_code == 200
    assert json.loads(response.content)["transformed_string"].startswith("MRS DALLOWAY")


#
# list-transformations endpoint tests
#


def test_list_transformations():
    """Testing the list transformations endpoint"""
    response = client.get("/v1/list-transformations")
    assert response.status_code == 200
