<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs tei" 
    version="2.0">
    
    <xsl:output omit-xml-declaration="yes" method="text" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
   
        <xsl:template match="list">
        <xsl:for-each select="item">
            <xsl:for-each select="collection(iri-to-uri(concat(@dir, '?select=*.xml')))">
                <xsl:variable name="outpath"
                    select="concat('../text/', substring-before(tokenize(document-uri(.), '/')[last()], '.xml'))"/>
                <xsl:result-document href="{concat($outpath, '.txt')}">
                    <xsl:apply-templates select="tei:TEI"/>
                </xsl:result-document>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>

    
    <xsl:template match="tei:teiHeader"/>
    
    <!-- applies to both element nodes and the root node -->
    <xsl:template match="*|/">
        <xsl:apply-templates/>
    </xsl:template>

    <!-- copies values of text and attribute nodes through -->
    <xsl:template match="text()|@*">
        <xsl:value-of select="."/>
    </xsl:template>

   
</xsl:stylesheet>
