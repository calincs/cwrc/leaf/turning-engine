"""
A module containing helper functions for xslt processing
"""

import os
from typing import Tuple
from saxonche import PySaxonProcessor

ALLOWED_TRANSFORMATIONS = {
    "TEI": {
        "HTML": {"XSL": "TEI2HTML.xsl"},
        "MD": {"XSL": "TEI2MD.xsl"},
    },
    "TRANSKRIBUS": {"TEI": {"XSL": "Transkribus2TEI.xsl"}},
    "WORD": {
        "TEI": {"XSL": "WORD2TEI.xsl"}
    },
}

EXT_TABLE = {
    "html": "html",
    "text": "txt",
    "xhtml": "xhtml",
    "xml": "xml",
    "json": "json",
    "markdown": "md",
}


# Helpful methods
def transformation_exists(from_type: str, to_type: str) -> bool:
    """Checks to see if the argument conversion is allowed"""
    return (
        from_type.upper() in ALLOWED_TRANSFORMATIONS
        and to_type.upper() in ALLOWED_TRANSFORMATIONS[from_type.upper()]
    )


async def transform_file(input_file: str, from_type: str, to_type: str) -> str:
    """Perform the xslt transformation as specified in the xslt directory"""
    if not transformation_exists(from_type, to_type):
        return ""

    return apply_xslt_from_file(
        input_file,
        os.path.dirname(os.path.abspath(__file__))
        + "/"
        + ALLOWED_TRANSFORMATIONS[from_type.upper()][to_type.upper()]["XSL"],
    )


# XSLT Processing Functions
def apply_xslt(input_string: str, xslt_file: dict) -> Tuple[str, str]:
    """Takes an xml file (str) and XSLT (str), then applies the XSL and returns a string"""

    with PySaxonProcessor(license=False) as proc:
        xslt_proc = proc.new_xslt30_processor()
        xpath_proc = proc.new_xpath_processor()

        # Load the XML file
        document = proc.parse_xml(xml_text=input_string)

        # Load the XSLT
        xslt_node = proc.parse_xml(**xslt_file)

        # Create a transformer object
        # xslt_executable = xslt_proc.compile_stylesheet(**xslt_file)
        xslt_executable = xslt_proc.compile_stylesheet(stylesheet_node=xslt_node)

        # Find the file extension from the XSLT for the result
        xpath_proc.set_context(xdm_item=xslt_node)
        xpath_proc.declare_namespace("xsl", "http://www.w3.org/1999/XSL/Transform")
        method_value = xpath_proc.evaluate_single(
            "/xsl:stylesheet/xsl:output/@method/data()"
        )
        file_extension = EXT_TABLE[str(method_value)]

        # Apply the transformation to the XML file
        result = xslt_executable.transform_to_string(xdm_node=document)

        # Output the transformed XML
        return str(result), file_extension


def apply_xslt_from_file(input_string: str, xslt_file: str) -> Tuple[str, str]:
    """Takes an xml file as str and XSLT as file, then applies the XSL and returns a string"""

    # Load the XSLT file
    # https://www.saxonica.com/saxon-c/doc12/html/saxonc.html#PyXslt30Processor
    # xslt_file = {"stylesheet_file": xslt_file}

    xslt_file = {"xml_file_name": xslt_file}
    return apply_xslt(input_string, xslt_file)


async def apply_xslt_from_string(input_string: str, xslt_body: str) -> Tuple[str, str]:
    """Takes an xml file as str and XSLT as str, then applies the XSL and returns a string"""

    # Load the XSLT file
    # https://www.saxonica.com/saxon-c/doc12/html/saxonc.html#PyXslt30Processor
    # xslt_file = {"stylesheet_text": xslt_body}

    xslt_file = {"xml_text": xslt_body}
    return apply_xslt(input_string, xslt_file)


# get_transformations returns dict as json string
def get_transformations():
    """Returns a dictionary of allowed transformations"""
    res = {}
    # iterating using items() to get key and value
    for key, value in ALLOWED_TRANSFORMATIONS.items():
        res[key] = list(value.keys())
    return res
