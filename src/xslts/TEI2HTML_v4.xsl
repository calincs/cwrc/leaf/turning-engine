<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
  xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml"
  xpath-default-namespace="http://www.tei-c.org/ns/1.0"
  xmlns:jc="http://james.blushingbunny.net/ns.html" exclude-result-prefixes="xs jc tei xhtml"
  version="2.0">

  <!-- This XSLT has been adapted from work done over time by members of the TEI-C council and community.
    Most recently it was developed specifically for work in the LEAF-Writer application by members of the Leaf Editorial Academic Framework (LEAF) team for use with the LEAF-Writer text-encoding enfironment.
    James Cummings (JC), Diane Jakacki (DJ), Mihaela Ilovan, and Bucknell students Viveka Kurup (Fall 2024) and Justin Verlin (Fall 2024).
    The LEAF-Writer project is licensed under the GNU Affero General Public License v3.0 (https://choosealicense.com/licenses/agpl-3.0/)
    For more information about LEAF-Writer, the larger LEAF platform, go to: https://gitlab.com/calincs/cwrc/leaf-writer/leaf-writer-->

  <!-- JC: Added in 'generateTOC' parameter, defaults to true -->
  <xsl:param name="generateTOC" select="'true'"/>

  <xsl:output doctype-public="-//W3C//:xhtmlDTD XHTML 1.0 Transitional//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" method="xhtml"
    omit-xml-declaration="yes" indent="yes" encoding="UTF-8"/>

  <!--<xsl:strip-space elements="*"/>-->

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="TEI">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8xsl:if"/>
        <xsl:comment>This document is generated from a TEI Master - do not edit!</xsl:comment>
        <title>
          <xsl:value-of select="teiHeader/fileDesc/titleStmt/title"/>
        </title>
        <link rel="stylesheet" type="text/css"
          href="https://github.com/LEAF-VRE/code_snippets/blob/main/CSS/leaf.css"/>
        <style type="text/css">
          @import url("https://github.com/LEAF-VRE/code_snippets/blob/main/CSS/leaf.css");</style>
      </head>
      <body>
        <!-- JC: if the generateTOC parameter is true, copy a table of contents here -->

        <xsl:if test="$generateTOC = 'true'">
          <xsl:copy-of select="jc:generateTOC(/)"/>
        </xsl:if>

        <!-- JC: adding xsl:for-each text to deal with instances where someone uses the group element or has multiple text elements  -->
        <xsl:for-each select=".//text">
          <xsl:apply-templates select="front"/>
          <xsl:apply-templates select="body"/>
          <xsl:apply-templates select="back"/>
        </xsl:for-each>

        <!-- DJ: Add notes section if there are any notes; this will appear at the bottom of the page -->
        <xsl:if test=".//note">
          <div class="notes-section">
            <h2>Notes</h2>
            <xsl:for-each select=".//note">
              <xsl:variable name="noteId" select="generate-id()"/>
              <xsl:variable name="noteNumber" select="count(preceding::note) + 1"/>
              <div class="note-item" id="{$noteId}">
                <span class="note-number">
                  <xsl:value-of select="$noteNumber"/>
                </span>
                <xsl:apply-templates/>
                <a href="#note-ref-{$noteId}" class="note-return"> ↩</a>
              </div>
            </xsl:for-each>
          </div>
        </xsl:if>

      </body>
    </html>
  </xsl:template>


  <xsl:template match="head">
    <!-- JC: added determining heading level by counting. Starting at h2 because assuming an overall title might be h1 -->
    <xsl:variable name="num">
      <xsl:value-of select="count(ancestor::div) + 1"/>
    </xsl:variable>
    <xsl:element name="h{$num}">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="div">
    <!-- JC: adding in creation of id, if it exists use the current @xml:id prefaced with TEI-id-, otherwise just generate one -->
    <xsl:variable name="id">
      <xsl:choose>
        <xsl:when test="./@xml:id">
          <xsl:value-of select="concat('TEI-id-', ./@xml:id)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="generate-id(.)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <div id="{$id}" class="TEI-div">
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <!-- DJ: the following templates manage paragraphs, line breaks, line groups (and lines), speeches, and quotes -->
  <xsl:template match="p">
    <p class="TEI-p">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="lb">
    <br class="TEI-lb"/>
  </xsl:template>

  <xsl:template match="lg">
    <p class="TEI-lg">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="l">
    <br class="TEI-l"/>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="sp">
    <p class="TEI-p">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="quote">
    <blockquote class="TEI-quote">
      <xsl:apply-templates/>
    </blockquote>
  </xsl:template>

  <!-- DJ: capture page break numbering and put a line break before and after the numbered page break -->
  <xsl:template match="pb[@n]">
    <br class="TEI-lb"/>
    <span class="TEI-pb">
      <xsl:text>Page: </xsl:text>
      <xsl:value-of select="@n"/>
      <xsl:apply-templates/>
    </span>
    <br class="TEI-lb"/>
  </xsl:template>

  <!-- DJ: create unordered lists and items -->
  <xsl:template match="list">
    <ul class="TEI-list">
      <xsl:apply-templates/>
    </ul>
  </xsl:template>

  <xsl:template match="list/item">
    <li class="TEI-item">
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <!-- DJ: the following templates manage manuscript features like deletions, super- and sub-scripts, underlines, emphasis, and added text above and below the line -->
  <xsl:template match="del">
    <del class="TEI-del">
      <xsl:apply-templates/>
    </del>
  </xsl:template>

  <xsl:template match="hi[@rend = 'superscript' or contains(@rend, 'sup')]">
    <sup class="TEI-hi-sup">
      <xsl:apply-templates/>
    </sup>
  </xsl:template>

  <xsl:template match="hi[@rend = 'underline' or contains(@rend, 'ul')]">
    <u class="TEI-hi ul">
      <xsl:apply-templates/>
    </u>
  </xsl:template>


  <xsl:template match="emph">
    <em class="TEI-emph">
      <xsl:apply-templates/>
    </em>
  </xsl:template>


  <xsl:template match="add[@place = 'above']">
    <sup class="TEI-add-above">
      <xsl:apply-templates/>
    </sup>
  </xsl:template>


  <xsl:template match="add[@place = 'below']">
    <sub class="TEI-add-below">
      <xsl:apply-templates/>
    </sub>
  </xsl:template>


  <!-- DJ: notes (of all kinds) are extracted from text and placed at the bottom of the web page with a reference number and link back to location in text -->
  <xsl:template match="note">
    <xsl:variable name="noteId" select="generate-id()"/>
    <xsl:variable name="noteNumber" select="count(preceding::note) + 1"/>
    <sup class="note-reference">
      <a href="#{$noteId}" id="note-ref-{$noteId}" class="note-reference">
        <xsl:value-of select="$noteNumber"/>
      </a>
    </sup>
  </xsl:template>

  <!-- DJ: entities (persons, organizations, places, titles) with @ref to authorities become weblinks -->
  <xsl:template match="persName[@ref] | orgName[@ref] | placeName[@ref] | title[@ref]">
    <a class="{concat('TEI-',name())}" href="{./@ref}">
      <xsl:if test="key">
        <xsl:attribute name="title">
          <xsl:value-of select="@key"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </a>
  </xsl:template>


  <!-- JC: catch-all for other TEI elements, just becomes an html span with a class of TEI-nameOfElement -->
  <xsl:template match="*[ancestor::text]" priority="-1">
    <span class="{concat('TEI-',name())}">
      <xsl:apply-templates/>
    </span>
  </xsl:template>

  <!-- DJ: creates table, rows, and cells -->
  <xsl:template match="tei:table">
    <table>
      <xsl:apply-templates/>
    </table>
  </xsl:template>

  <xsl:template match="tei:row">
    <tr>
      <xsl:apply-templates/>
    </tr>
  </xsl:template>

  <xsl:template match="tei:cell">
    <td>
      <xsl:apply-templates/>
    </td>
  </xsl:template>

  <!-- JC: TOC mode templates -->
  <xsl:template match="div[head]" mode="TOC">
    <xsl:variable name="id">
      <xsl:choose>
        <xsl:when test="./@xml:id">
          <xsl:value-of select="concat('TEI-id-', ./@xml:id)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="generate-id(.)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <li>
      <a href="{concat('#', $id)}">
        <xsl:apply-templates select="head" mode="TOC"/>
      </a>
      <!-- nested divs -->
      <xsl:if test=".//div[head]">
        <ul>
          <xsl:apply-templates select="./div[head]" mode="TOC"/>
        </ul>
      </xsl:if>
    </li>
  </xsl:template>

  <!-- JC: in case the <head> has element content which the XSLT sorts out in some way -->
  <xsl:template match="head" mode="TOC">
    <xsl:apply-templates/>
  </xsl:template>



  <!-- functions -->

  <!-- JC: function to generate a table of contents it only looks for divs which have heads, and needs the div and head templates in 
    mode="TOC" above. -->
  <xsl:function name="jc:generateTOC" as="item()*">
    <xsl:param name="root"/>
    <xsl:if test="$root//div[head]">
      <xsl:comment>
                Generated Table of Contents
            </xsl:comment>
      <div class="TOC" id="TOC">
        <h2 class="TOC-heading">Table of Contents</h2>
        <!-- JC: there might be multiple text elements, the TOC should do front|body|back for each of these. -->
        <xsl:for-each select="$root//text">
          <xsl:if test="front/div[.//head]">
            <xsl:comment>Front Matter</xsl:comment>
            <ul class="TOC-front-matter">
              <xsl:apply-templates select="front/div" mode="TOC"/>
            </ul>
          </xsl:if>
          <xsl:if test="body/div[.//head]">
            <xsl:comment>Body</xsl:comment>
            <ul class="TOC-body">
              <xsl:apply-templates select="body/div" mode="TOC"/>
            </ul>
          </xsl:if>
          <xsl:if test="back/div[.//head]">
            <xsl:comment>Back Matter</xsl:comment>
            <ul class="TOC-back">
              <xsl:apply-templates select="back/div" mode="TOC"/>
            </ul>
          </xsl:if>
        </xsl:for-each>
      </div>
    </xsl:if>
  </xsl:function>

</xsl:stylesheet>
