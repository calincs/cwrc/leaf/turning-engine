<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage"
    xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
    xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
    xmlns="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="#all">
    
    <!-- This XSLT has been adapted from work done over time by members of the TEI-C council and community.
    Most recently it was developed specifically to transform the XML output from WORD XML to
    to TEI-All by members of the Leaf Editorial Academic Framework (LEAF) team for use with the LEAF-Writer text-encoding enfironment.
    The XSLT was enhanced with assistance from Claude (Anthropic, 2024)
    The LEAF-Writer project is licensed under the GNU Affero General Public License v3.0 (https://choosealicense.com/licenses/agpl-3.0/)
    For more information about LEAF-Writer, the larger LEAF platform, go to: https://gitlab.com/calincs/cwrc/leaf-writer/leaf-writer-->
    
    <!-- To use this transformation, the Word document must be saved as a Word XML file (in the MS Word application). -->
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    
    <!-- Root template -->
    <xsl:template match="/">
        <xsl:text>&#xa;</xsl:text>
        <xsl:processing-instruction name="xml-model">href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"</xsl:processing-instruction>
        <xsl:text>&#xa;</xsl:text>
        <xsl:processing-instruction name="xml-model">href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
        <xsl:text>&#xa;</xsl:text>
        
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                <fileDesc>
                    <titleStmt>
                        <title>
                            <xsl:value-of select="//w:p[w:pPr/w:pStyle[@w:val='Heading1']][1]"/>
                        </title>
                    </titleStmt>
                    <publicationStmt>
                        <publisher/>
                        <availability><p>Published under unspecified terms</p></availability>
                        <date><xsl:value-of select="current-date()"/></date>
                    </publicationStmt>
                    <sourceDesc><p>Converted from DOCX</p></sourceDesc>
                </fileDesc>
                <encodingDesc>
                    <projectDesc>
                        <p>Converted from DOCX to TEI using XSLT</p>
                    </projectDesc>
                </encodingDesc>
            </teiHeader>
            <text>
                <body>
                    <div>
                        <xsl:for-each-group select="//w:body/*" 
                            group-adjacent="if (self::w:p[w:pPr/w:pStyle[@w:val[starts-with(., 'Heading')]]])
                            then 'heading'
                            else if (self::w:p[w:pPr/w:numPr])
                            then 'list'
                            else if (self::w:tbl)
                            then 'table'
                            else 'content'">
                            
                            <xsl:choose>
                                <!-- Handle headings/sections -->
                                <xsl:when test="current-grouping-key() = 'heading'">
                                    <xsl:for-each select="current-group()">
                                        <div type="section">
                                            <head>
                                                <xsl:attribute name="type">
                                                    <xsl:value-of select="w:pPr/w:pStyle/@w:val"/>
                                                </xsl:attribute>
                                                <xsl:apply-templates select="w:r|w:hyperlink"/>
                                            </head>
                                        </div>
                                    </xsl:for-each>
                                </xsl:when>
                                
                                <!-- Handle lists -->
                                <xsl:when test="current-grouping-key() = 'list'">
                                    <div>
                                        <list>
                                            <xsl:for-each select="current-group()">
                                                <item>
                                                    <xsl:apply-templates select="w:r|w:hyperlink"/>
                                                </item>
                                            </xsl:for-each>
                                        </list>
                                    </div>
                                </xsl:when>
                                
                                <!-- Handle tables -->
                                <xsl:when test="current-grouping-key() = 'table'">
                                    <div>
                                        <xsl:apply-templates select="current-group()"/>
                                    </div>
                                </xsl:when>
                                
                                <!-- Handle regular paragraphs -->
                                <xsl:when test="current-grouping-key() = 'content'">
                                    <div>
                                        <xsl:apply-templates select="current-group()"/>
                                    </div>
                                </xsl:when>
                            </xsl:choose>
                            
                        </xsl:for-each-group>
                    </div>
                </body>
            </text>
        </TEI>
    </xsl:template>
    
    <!-- Paragraph template -->
    <xsl:template match="w:p">
        <xsl:if test="normalize-space(string-join(.//text(), ''))">
            <p>
                <xsl:apply-templates select="w:r|w:hyperlink"/>
            </p>
        </xsl:if>
    </xsl:template>
    
    <!-- Table template -->
    <xsl:template match="w:tbl">
        <table>
            <xsl:apply-templates select="w:tr"/>
        </table>
    </xsl:template>
    
    <!-- Table row template -->
    <xsl:template match="w:tr">
        <row>
            <xsl:apply-templates select="w:tc"/>
        </row>
    </xsl:template>
    
    <!-- Table cell template -->
    <xsl:template match="w:tc">
        <cell>
            <xsl:apply-templates select="w:p"/>
        </cell>
    </xsl:template>
    
    <!-- Text run template -->
    <xsl:template match="w:r">
        <xsl:variable name="styles">
            <xsl:if test="w:rPr/w:i">italic</xsl:if>
            <xsl:if test="w:rPr/w:b">bold</xsl:if>
            <xsl:if test="w:rPr/w:u">underline</xsl:if>
        </xsl:variable>
        
        <xsl:choose>
            <xsl:when test="normalize-space($styles)">
                <hi rend="{normalize-space($styles)}">
                    <xsl:apply-templates select="w:t"/>
                </hi>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="w:t"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Text template -->
    <xsl:template match="w:t">
        <xsl:value-of select="."/>
    </xsl:template>
    
    <!-- Hyperlink template -->
    <xsl:template match="w:hyperlink">
        <ref type="link">
            <xsl:variable name="text">
                <xsl:value-of select=".//w:t"/>
            </xsl:variable>
            
            <xsl:choose>
                <xsl:when test="starts-with($text, 'http')">
                    <xsl:attribute name="target">
                        <xsl:value-of select="$text"/>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@w:anchor">
                    <xsl:attribute name="target">
                        <xsl:value-of select="@w:anchor"/>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
            
            <xsl:apply-templates select=".//w:t"/>
        </ref>
    </xsl:template>
    
</xsl:stylesheet>