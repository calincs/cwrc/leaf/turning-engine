# Checkout this documentation to find more infomation of big application Dockerfiles
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
# And https://fastapi.tiangolo.com/deployment/docker/#bigger-applications

# Optimized for running FastAPI applications and includes pre-configured server and security settings
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.11

# Setting up working directory
WORKDIR /app

# Copying and Installing requirements
COPY ./requirements/requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

# Copying Source Code
COPY ./src /app/src

# Final container execution command
CMD ["uvicorn", "src.main:app", "--workers", "4", "--host", "0.0.0.0", "--port", "80"]

# Uncomment for self signed SSL
# CMD ["uvicorn", "src.main:app", "--workers", "4", "--host", "0.0.0.0", "--port", "443", "--ssl-keyfile", "/etc/ssl/key.pem", "--ssl-certfile", "/etc/ssl/cert.pem"]
