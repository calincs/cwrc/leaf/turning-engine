# Leaf Turning Engine

XSLT API Processor

## How it works :mag:

The conversion API provides a single dynamic endpoint called /v1/transform that handles all the document transformations. The endpoint accepts requests in the form of HTTP POST requests, and returns responses in the form of JSON objects.

To transform a document, the client sends a POST request to the /v1/transform endpoint with the following parameters:

    file: The file to be transformed
    from_type: The input document format
    to_type: The output document format

or

    file: The file to be transformed
    xslt: The XSLT file to use for the transformation, if required.

The API then applies the necessary transformations to convert the input document to the desired output format. If an XSLT file is specified, the API uses it to apply the necessary transformations. Otherwise, it applies the default transformations specified for the given input and output document formats.

For example, to convert a TEI document to HTML, the client would send a POST request to the /v1/transform endpoint with the from_type parameter set to "TEI", the to_type parameter set to "HTML", and the file parameter set to the TEI document to be transformed. The API would then apply the necessary transformations to convert the TEI document to HTML format, and return the transformed document as a JSON object in the response.

Similarly, to convert a Transkribus document to TEI, the client would send a POST request to the /v1/transform endpoint with the from_type parameter set to "Transkribus", the to_type parameter set to "TEI", and the file parameter set to the Transkribus document to be transformed. The API would then apply the necessary transformations to convert the Transkribus document to TEI format, and return the transformed document as a JSON object in the response.

By using this approach, the conversion API allows clients to easily transform documents between different formats without needing to know the details of the underlying transformation process.

## Installation :wrench:

Running a virtual environment locally is recommended. To achieve this, execute the following command.

### Creating a virtual environment :computer:

    python3 -m venv venv
    source venv/bin/activate

## Installing Dependencies :package:

    python3 -m pip install -r requirements/requirements.txt

## Running The App :running_woman:

    uvicorn src.main:app --reload

## How to Test & Lint :white_check_mark:

To have the code ready for production, simply run:

    nox

If you would like to test separately, run:

    nox -s test

and if you would like to lint separately, run:

    nox -s lint

## How our CI/CD Setup Works :arrows_counterclockwise:

Our CI/CD (Continuous Integration/Continuous Deployment) setup is designed to ensure that our code is consistently tested, built, and deployed to our production environment. We use Github Actions to automate these processes and ensure that our code is always of the highest quality.

### More On Testing and Linting

We use Github Actions to run automated tests and linters on every code commit. This allows us to catch potential issues early on in the development process and ensure that our code is consistent and meets our quality standards.

Our testing suite includes both unit tests and integration tests, which run on each commit. Additionally, we use linters to check our code for issues related to style, formatting, and potential bugs.

### Dockerization :whale:

We also use Github Actions to Dockerize our applications. Dockerization helps to streamline the deployment process and ensure that our applications run consistently across different environments.

Our Dockerization process includes building a Docker image of our application, which we can then deploy to our production environment. This helps to ensure that our application runs smoothly and consistently in a containerized environment.

### Deployment :rocket:

Finally, we use Github Actions to automate our deployment process. Whenever we push changes to our main branch, our CI/CD pipeline is triggered, and our code is automatically deployed to our production environment.

Overall, our CI/CD setup helps us to ensure that our code is consistently tested and deployed, allowing us to deliver high-quality applications to our users.

## API Documentation :notebook_with_decorative_cover:

More specific API documentation comes with FastAPI's /docs endpoint which can be found once the server is running at:

<http://127.0.0.1:8000/docs>

## Example Requests (Javascript & Python) :globe_with_meridians:

### Javascript :coffee:

**This example uses Javascript within an HTML file and assumes the document you're uploading is the ExampleTranskribus.xml file.**

```html
<!DOCTYPE html>
<html>
<head>
  <title>File Upload Example</title>
</head>
<body>
  <input type="file" id="fileInput">
  <script>
  const fileInput = document.getElementById("fileInput"); // Replace with the ID of your file input element
  const apiUrl = "https://leaf-turning.leaf-vre.org/v1/transform-file"; // Replace with the actual API endpoint

fileInput.addEventListener("change", async (event) => {
  const file = event.target.files[0];
  const formData = new FormData();
  formData.append("file", file, "ExampleTranskribus.xml");
  formData.append("from_type", "TRANSKRIBUS");
  formData.append("to_type", "TEI");

  try {
    const response = await fetch(apiUrl, {
      method: "POST",
      body: formData,
    });

    if (response.ok) {
      console.log("Request successful");
      const text = await response.text();
      console.log(text);
      const paragraph = document.getElementById("converted");
      paragraph.innerHTML += text;
    } else {
      console.error("Request failed with status code", response.status);
      const errorText = await response.text(); // Use response.text() to access the response body as text
      console.error(errorText);
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
});
  </script>
  <p id="converted"></p>
</body>
</html>
```

### Python :snake:
**This example uses the requests module. To install it, use:**

```
sudo pip3 install requests
```

With the requests module installed, you can easily access the API with a simple call:

```python
import os
import requests

# Construct file path
file_path = os.path.join(os.path.dirname(__file__), "./src/test/ExampleTranskribus.xml")

# Open the file in binary mode
with open(file_path, "rb") as file:
    # Prepare the file and additional parameters for the request
    files = {
        "file": ("ExampleTranskribus.xml", file, "multipart/form-data"),
        "from_type": (None, "TRANSKRIBUS"),
        "to_type": (None, "TEI"),
    }

    # Send the POST request with files and additional parameters
    response = requests.post(
        "https://leaf-turning.leaf-vre.org/v1/transform-file",  # Update with the actual API endpoint
        files=files
    )

# Check the response status code and content
if response.status_code == 200:
    print("Request successful")
    print(response.content.decode())  # Use response.json() to access the response body as JSON
else:
    print("Request failed with status code", response.status_code)
    print(response.text)  # Use response.text to access the response body as text

```

## Error Codes :warning:

The following error codes are thrown by the API:

### 400
This error is thrown for the following reasons:
- Inccorrect transformation params (from_type and to_type are values that cannot be transformed)
    - Example: from_type = TEI and to_type = pdf
    - Example: to_type is not passed but from_type = TEI (and vice-versa)
- Input file/string is of type that cannot be transformed (.png)

### 422
This error is thrown for the following reasons:
- No file/string sent
- Uploading a file to the /docs endpoint, followed by resetting the form and executing.

### 408
This error code is designed to enforce DOS protection measures. It is triggered when the total execution time of the code exceeds 45 seconds.

